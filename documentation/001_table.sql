-- Passage à UnicaenEtat

create table unicaen_etat_categorie
(
    id                      serial          not null constraint unicaen_etat_categorie_pk primary key,
    code                    varchar(256)    not null,
    libelle                 varchar(256)    not null,
    icone                   varchar(256),
    couleur                 varchar(256),
    ordre                   integer
);
create unique index unicaen_etat_categorie_id_uindex on unicaen_etat_categorie (id);

create table unicaen_etat_type
(
    id                      serial          not null constraint unicaen_etat_type_pk primary key,
    code                    varchar(256)    not null,
    libelle                 varchar(256)    not null,
    categorie_id            integer         constraint unicaen_etat_type_categorie_id_fk references unicaen_etat_categorie,
    icone                   varchar(256),
    couleur                 varchar(256),
    ordre                   integer         not null default 9999
);
create unique index unicaen_etat_type_id_uindex on unicaen_etat_type (id);

create table unicaen_etat_instance
(
    id                      serial          constraint unicaen_etat_instance_pk primary key,
    type_id                 integer         not null constraint unicaen_etat_instance_type_id references unicaen_etat_type,
    infos                   text            default null,
    histo_creation          timestamp       not null,
    histo_createur_id       integer         not null constraint unicaen_content_content_user_id_fk references unicaen_utilisateur_user,
    histo_modification      timestamp,
    histo_modificateur_id   integer         constraint unicaen_content_content_user_id_fk_2 references unicaen_utilisateur_user,
    histo_destruction       timestamp,
    histo_destructeur_id    integer         constraint unicaen_content_content_user_id_fk_3 references unicaen_utilisateur_user
);
create unique index unicaen_etat_instance_id_index on unicaen_etat_instance (id);



