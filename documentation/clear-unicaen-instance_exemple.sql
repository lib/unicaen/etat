
-- Procédure permettant de supprimer automatiquement de unicaen_etat_instance des instance qui ne sont plus rattaché à rien
create or replace procedure clear_etat_instance()
    language plpgsql
as
$$
DECLARE
BEGIN

    with instances_lier as (
        select etat_instance_id from table_linker_1
        union select etat_instance_id from table_linker_2
        union select etat_instance_id from table_linker_3
              ...
    )
       , to_delete as (
        select i.id from unicaen_etat_instance i
                             left join instances_lier l on l.etat_instance_id = i.id
        where l.etat_instance_id is null
    )
    delete from unicaen_etat_instance where id in (select * from to_delete);
-- Remise au propre de la séquence
    perform SETVAL('unicaen_etat_instance_id_seq', COALESCE(MAX(id), 1) ) FROM unicaen_etat_instance;
    perform SETVAL('unicaen_validation_instance_id_seq', COALESCE(MAX(id), 1) ) FROM unicaen_validation_instance;
end;
$$;

call clear_etat_instance();