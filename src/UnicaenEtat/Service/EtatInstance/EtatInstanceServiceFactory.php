<?php

namespace UnicaenEtat\Service\EtatInstance;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use UnicaenEtat\Service\EtatType\EtatTypeService;

class EtatInstanceServiceFactory {

    /**
     * @param ContainerInterface $container
     * @return EtatInstanceService
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : EtatInstanceService
    {
        /**
         * @var EntityManager $entityManager
         * @var EtatTypeService $etatTypeService
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $etatTypeService = $container->get(EtatTypeService::class);

        $service = new EtatInstanceService();
        $service->setEntityManager($entityManager);
        $service->setEtatTypeService($etatTypeService);
        return $service;
    }
}