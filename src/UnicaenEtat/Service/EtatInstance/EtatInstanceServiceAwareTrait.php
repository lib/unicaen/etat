<?php

namespace UnicaenEtat\Service\EtatInstance;

trait EtatInstanceServiceAwareTrait
{
    private EtatInstanceService $etatInstanceService ;

    /**
     * @return EtatInstanceService
     */
    public function getEtatInstanceService(): EtatInstanceService
    {
        return $this->etatInstanceService;
    }

    /**
     * @param EtatInstanceService $etatInstanceService
     */
    public function setEtatInstanceService(EtatInstanceService $etatInstanceService): void
    {
        $this->etatInstanceService = $etatInstanceService;
    }

}