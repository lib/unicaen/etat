<?php

namespace UnicaenEtat\Service\EtatInstance;

use Doctrine\ORM\Exception\NotSupported;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenApp\Exception\RuntimeException;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenEtat\Entity\Db\EtatInstance;
use UnicaenEtat\Entity\Db\HasEtatsInterface;
use UnicaenEtat\Service\EtatType\EtatTypeServiceAwareTrait;
use UnicaenUtilisateur\Entity\Db\User;

class EtatInstanceService {
    use EntityManagerAwareTrait;
    use EtatTypeServiceAwareTrait;

    /** GESTION DES ENTITES ***************************************************************************/

    public function create(EtatInstance $instance) : EtatInstance
    {
        try {
            $this->getEntityManager()->persist($instance);
            $this->getEntityManager()->flush($instance);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu en bd", 0, $e);
        }
        return $instance;
    }

    public function update(EtatInstance $instance) : EtatInstance
    {
        try {
            $this->getEntityManager()->flush($instance);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu en bd", 0, $e);
        }
        return $instance;
    }

    public function historise(EtatInstance $instance) : EtatInstance
    {
        try {
            $instance->historiser();
            $this->getEntityManager()->flush($instance);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu en bd", 0, $e);
        }
        return $instance;
    }

    public function restore(EtatInstance $instance) : EtatInstance
    {
        try {
            $instance->dehistoriser();
            $this->getEntityManager()->flush($instance);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu en bd", 0, $e);
        }
        return $instance;
    }

    public function delete(EtatInstance $instance) : EtatInstance
    {
        try {
            $this->getEntityManager()->remove($instance);
            $this->getEntityManager()->flush($instance);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu en bd", 0, $e);
        }
        return $instance;
    }

    /** REQUETAGE **********************************************************************************/

    /**
     * @return QueryBuilder
     */
    public function createQueryBuilder() : QueryBuilder
    {
        try {
            $qb = $this->getEntityManager()->getRepository(EtatInstance::class)->createQueryBuilder('einstance')
                ->addSelect('createur')->join('einstance.histoCreateur', 'createur')
                ->addSelect('modificateur')->join('einstance.histoModificateur', 'modificateur')
                ->join('einstance.type', 'etype')->addSelect('etype')
                ->leftJoin('etype.categorie', 'ecategorie')->addSelect('ecategorie');
        } catch (NotSupported $e) {
            throw new RuntimeException("Un problème est survenu lors de la création du QueryBuilder de [".EtatInstance::class."]",0,$e);
        }
        return $qb;
    }

    /**
     * @param int|null $id
     * @return EtatInstance|null
     */
    public function getEtatInstance(?int $id) : ?EtatInstance
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('einstance.id = :id')
            ->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException('Plusieurs EtatInstance partagent le même id ['.$id.']',0,$e);
        }

        return $result;
    }

    /**
     * @param AbstractActionController $controller
     * @param string $param
     * @return EtatInstance|null
     */
    public function getResquestedEtatInstance(AbstractActionController $controller, string $param='instance') : ?EtatInstance
    {
        $id = $controller->params()->fromRoute($param);
        $result = $this->getEtatInstance($id);
        return $result;
    }

    /**
     * @param string $champ
     * @param string $ordre
     * @return EtatInstance[]
     */
    public function getEtatsInstances(string $champ = 'histoCreation', string $ordre = 'ASC') : array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('einstance.'.$champ, $ordre);
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param ?string $term
     * @return User[]
     */
    public function findUtilisateurByTerm(?string $term) : array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere("LOWER(createur.displayName) like :search")
            ->setParameter('search', '%'.strtolower($term).'%');
        $result = $qb->getQuery()->getResult();

        $validateurs = [];
        /** @var  $item */
        foreach ($result as $item) {
            $validateurs[$item->getHistoCreateur()->getId()] = $item;
        }
        return $validateurs;
    }

    /**
     * @param array $params
     * @return EtatInstance[]
     */
    public function getEtatsInstancesWithFiltre(array $params) : array
    {
        $qb = $this->createQueryBuilder();
        if (isset($params['type'])  AND $params['type'] !== '') {
            $qb = $qb->andWhere('etype.code = :code')->setParameter('code', $params['type']);
        }
        if (isset($params['utilisateur']) AND isset($params['utilisateur']['id']) AND $params['utilisateur']['id'] !== '') {
            $id = ((int) $params['utilisateur']['id']);
            $qb = $qb->andWhere('createur.id = :id')->setParameter('id', $id);
        }

        $result = $qb->getQuery()->getResult();
        return $result;
    }
    /** FACADE ********************************************************************************************************/

    public function createWithCode(string $codeType, ?string $codeCategorie = null) : EtatInstance
    {
        $type = $this->getEtatTypeService()->getEtatTypeByCode($codeType, $codeCategorie);

        $instance = new EtatInstance();
        $instance->setType($type);
        $this->create($instance);

        return $instance;
    }

    public function setEtatActif(HasEtatsInterface $element, string $codeType, ?string $categorieCode = null, bool $flush = true) : HasEtatsInterface
    {

        $etatActif = $element->getEtatActif();
        //Si l'état ne change pas on ne fait rien
        if(isset($etatActif) && $etatActif->getType()->getCode() == $codeType){
            return $element;
        }

        //historisation de l'état actif actuel (s'il existe)
        if (isset($etatActif)) {
            $this->historise($etatActif);
        }

        //creation de la nouvelle instance
        $etat = $this->createWithCode($codeType);

        //ajout à l'element et persist
        $element->addEtat($etat);
        try {
            $this->getEntityManager()->persist($element);
            if ($flush === true) $this->getEntityManager()->flush($element);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de l'ajout de l'état actif de [".get_class($element)."]",0,$e);
        }
        return $element;
    }
}