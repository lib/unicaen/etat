<?php

namespace UnicaenEtat\Service\EtatCategorie;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class EtatCategorieServiceFactory {

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : EtatCategorieService
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new EtatCategorieService();
        $service->setEntityManager($entityManager);
        return $service;
    }
}