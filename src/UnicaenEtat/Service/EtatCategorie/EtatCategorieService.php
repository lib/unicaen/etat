<?php

namespace UnicaenEtat\Service\EtatCategorie;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;
use UnicaenApp\Exception\RuntimeException;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenEtat\Entity\Db\EtatCategorie;
use Laminas\Mvc\Controller\AbstractActionController;

class EtatCategorieService {
    use EntityManagerAwareTrait;

    /** GESTION DES ENTITES  ******************************************************************************************/

    /**
     * @param EtatCategorie $type
     * @return EtatCategorie
     */
    public function create(EtatCategorie $type) : EtatCategorie
    {
        try {
            $this->getEntityManager()->persist($type);
            $this->getEntityManager()->flush($type);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $type;
    }

    /**
     * @param EtatCategorie $type
     * @return EtatCategorie
     */
    public function update(EtatCategorie $type) : EtatCategorie
    {
        try {
            $this->getEntityManager()->flush($type);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $type;
    }

    /**
     * @param EtatCategorie $type
     * @return EtatCategorie
     */
    public function delete(EtatCategorie $type) : EtatCategorie
    {
        try {
            $this->getEntityManager()->remove($type);
            $this->getEntityManager()->flush($type);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $type;
    }

    /** REQUETAGE *****************************************************************************************************/

    /**
     * @return QueryBuilder
     */
    public function createQueryBuilder() : QueryBuilder
    {
        $qb = $this->getEntityManager()->getRepository(EtatCategorie::class)->createQueryBuilder('ecategorie')
        ;
        return $qb;
    }

    /**
     * @param string $champ
     * @param string $ordre
     * @return EtatCategorie[]
     */
    public function getEtatsCategories(string $champ = 'ordre', string $ordre='ASC') : array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('ecategorie.'.$champ, $ordre)
        ;
        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $champ
     * @param string $ordre
     * @return array
     */
    public function getEtatsCategoriesAsOptions(string $champ = 'ordre', string $ordre = 'ASC') : array
    {
        $result = $this->getEtatsCategories($champ,$ordre);
        $array = [];
        foreach ($result as $item) {
            $array[$item->getCode()] = $item->getLibelle();
        }
        return $array;
    }

    /**
     * @param int $id
     * @return EtatCategorie|null
     */
    public function getEtatCategorie(int $id) : ?EtatCategorie
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('ecategorie.id = :id')
            ->setParameter('id', $id)
        ;
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs EtatCategorie partagent le même id [".$id."]");
        }
        return $result;
    }

    /**
     * @param AbstractActionController $controller
     * @param string $param
     * @return EtatCategorie|null
     */
    public function getRequestedEtatCategorie(AbstractActionController $controller, string $param='categorie') : ?EtatCategorie
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getEtatCategorie($id);
    }

    /**
     * @param string $code
     * @return EtatCategorie|null
     */
    public function getEtatCategorieByCode(string $code) : ?EtatCategorie
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('ecategorie.code = :code')
            ->setParameter('code', $code)
        ;
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs EtatCategorie partagent le même code [".$code."]");
        }
        return $result;
    }
}