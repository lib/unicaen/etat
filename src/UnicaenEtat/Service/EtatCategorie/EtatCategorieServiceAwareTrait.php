<?php

namespace UnicaenEtat\Service\EtatCategorie;

trait EtatCategorieServiceAwareTrait {

    /** @var EtatCategorieService */
    private $etatCategorieService;

    /**
     * @return EtatCategorieService
     */
    public function getEtatCategorieService(): EtatCategorieService
    {
        return $this->etatCategorieService;
    }

    /**
     * @param EtatCategorieService $etatCategorieService
     * @return EtatCategorieService
     */
    public function setEtatCategorieService(EtatCategorieService $etatCategorieService): EtatCategorieService
    {
        $this->etatCategorieService = $etatCategorieService;
        return $this->etatCategorieService;
    }


}