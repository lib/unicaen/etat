<?php

namespace UnicaenEtat\Service\EtatType;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;
use Laminas\View\Renderer\PhpRenderer;
use UnicaenApp\Exception\RuntimeException;
use UnicaenApp\Service\EntityManagerAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenEtat\Entity\Db\EtatCategorie;
use UnicaenEtat\Entity\Db\EtatType;

class EtatTypeService {
    use EntityManagerAwareTrait;

    private PhpRenderer $renderer;
    public function setRenderer($renderer) {$this->renderer = $renderer;}

    /** GESTION DES ENTITES *******************************************************************************************/

    /**
     * @param EtatType $etatType
     * @return EtatType
     */
    public function create(EtatType $etatType) : EtatType
    {
        try {
            $this->getEntityManager()->persist($etatType);
            $this->getEntityManager()->flush($etatType);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $etatType;
    }

    /**
     * @param EtatType $etatType
     * @return EtatType
     */
    public function update(EtatType $etatType) : EtatType
    {
        try {
            $this->getEntityManager()->flush($etatType);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $etatType;
    }

    /**
     * @param EtatType $etatType
     * @return EtatType
     */
    public function delete(EtatType $etatType) : EtatType
    {
        try {
            $this->getEntityManager()->remove($etatType);
            $this->getEntityManager()->flush($etatType);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $etatType;
    }

    /** REQUETAGE *****************************************************************************************************/

    /**
     * @return QueryBuilder
     */
    public function createQueryBuilder() : QueryBuilder
    {
        $qb = $this->getEntityManager()->getRepository(EtatType::class)->createQueryBuilder('etype')
            ->addSelect('ecategorie')->leftjoin('etype.categorie', 'ecategorie')
        ;
        return $qb;
    }

    /**
     * @param string $champ
     * @param string $ordre
     * @return EtatType[]
     */
    public function getEtatsTypes(string $champ = 'code', string $ordre = 'ASC') : array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('etype.categorie, etype.ordre', $ordre)
        ;
        return $qb->getQuery()->getResult();
    }

    /**
     * @param int|null $id
     * @return EtatType|null
     */
    public function getEtatType(?int $id) : ?EtatType
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('etype.id = :id')
            ->setParameter('id', $id)
        ;
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs EtatType partagent le même id [".$id."].");
        }
        return $result;
    }

    public function getEtatTypeByCode(string $typeCode, ?string $categorieCode=null) : ?EtatType
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('etype.code = :typeCode')->setParameter('typeCode', $typeCode);
        if ($categorieCode !== null) {
            $qb = $qb->andWhere('ecategorie.code = :categorieCode')->setParameter('categorieCode',$categorieCode);
        }

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs EtatType partagent le même code [".$typeCode.(($categorieCode)?("|".$categorieCode):"")."].");
        }
        return $result;
    }

    /**
     * @param AbstractActionController $controller
     * @param string $param
     * @return EtatType|null
     */
    public function getRequestedEtatType(AbstractActionController $controller, string $param='type') : ?EtatType
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getEtatType($id);
    }

    /**
     * @param ?EtatCategorie $categorie
     * @return EtatType[]
     */
    public function getEtatsTypesByCategorie(?EtatCategorie $categorie = null) : array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('etype.categorie = :categorie')
            ->setParameter('categorie', $categorie)
            ->orderBy('etype.ordre', 'ASC')
        ;
        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $code
     * @return EtatType[]
     */
    public function getEtatsTypesByCategorieCode(string $code) : array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('ecategorie.code = :code')
            ->setParameter('code', $code)
            ->orderBy('ecategorie.ordre, etype.ordre', 'ASC')
        ;
        /** @var EtatType[] $temp */
        $temp =  $qb->getQuery()->getResult();

        $array = [];
        foreach ($temp as $etattype) {
            $array[$etattype->getCode()] = $etattype;
        }
        return $array;
        
    }

    /**
     * @param EtatCategorie|null $categorie
     * @return array
     */
    public function getEtatTypesAsOption(?EtatCategorie $categorie = null) : array
    {
        $etatTypes = [];
        if ($categorie === null) {
            $etatTypes = $this->getEtatsTypes();
        } else {
            $etatTypes = $this->getEtatsTypesByCategorie($categorie);
        }

        $options = [];
        foreach ($etatTypes as $etatType) {
            $options[$etatType->getId()] = $this->optionify($etatType);
        }
        return $options;
    }

    /**
     * @param EtatType $etatType
     * @return array
     */
    public function optionify(EtatType $etatType) : array
    {
        $res = $this->renderer->etattype($etatType);

        $this_option = [
            'value' =>  $etatType->getId(),
            'attributes' => [
                'data-content' => $res . "&nbsp;&nbsp;&nbsp;&nbsp;".$etatType->getLibelle(),
            ],
            'label' => $etatType->getLibelle(),
        ];
        return $this_option;
    }
}