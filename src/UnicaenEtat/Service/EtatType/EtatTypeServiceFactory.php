<?php

namespace UnicaenEtat\Service\EtatType;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Laminas\View\Renderer\PhpRenderer;

class EtatTypeServiceFactory {

    /**
     * @param ContainerInterface $container
     * @return EtatTypeService
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : EtatTypeService
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        /* @var PhpRenderer $renderer  */
        $renderer = $container->get('ViewRenderer');

        $service = new EtatTypeService();
        $service->setRenderer($renderer);
        $service->setEntityManager($entityManager);
        return $service;
    }
}