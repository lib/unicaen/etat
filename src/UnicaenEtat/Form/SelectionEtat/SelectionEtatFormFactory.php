<?php

namespace UnicaenEtat\Form\SelectionEtat;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEtat\Service\EtatType\EtatTypeService;

class SelectionEtatFormFactory
{

    /**
     * @param ContainerInterface $container
     * @return SelectionEtatForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SelectionEtatForm
    {
        /**
         * @var EtatTypeService $etatTypeService
         */
        $etatTypeService = $container->get(EtatTypeService::class);

        /**
         * @var SelectionEtatHydrator $hydrator
         */
        $hydrator = $container->get('HydratorManager')->get(SelectionEtatHydrator::class);

        $form = new SelectionEtatForm();
        $form->setEtatTypeService($etatTypeService);
        $form->setHydrator($hydrator);
        return $form;
    }
}