<?php

namespace UnicaenEtat\Form\SelectionEtat;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEtat\Service\EtatInstance\EtatInstanceService;
use UnicaenEtat\Service\EtatType\EtatTypeService;

class SelectionEtatHydratorFactory {

    /**
     * @param ContainerInterface $container
     * @return SelectionEtatHydrator
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SelectionEtatHydrator
    {
        /**
         * @var EtatInstanceService $etatInstanceService
         * @var EtatTypeService $etatTypeService
         */
        $etatTypeService = $container->get(EtatTypeService::class);
        $etatInstanceService = $container->get(EtatInstanceService::class);

        $hydrator = new SelectionEtatHydrator();
        $hydrator->setEtatTypeService($etatTypeService);
        $hydrator->setEtatInstanceService($etatInstanceService);
        return $hydrator;
    }

}