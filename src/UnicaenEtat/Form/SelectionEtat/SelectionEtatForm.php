<?php

namespace UnicaenEtat\Form\SelectionEtat;

use UnicaenEtat\Entity\Db\EtatCategorie;
use UnicaenEtat\Entity\Db\EtatType;
use UnicaenEtat\Service\EtatType\EtatTypeServiceAwareTrait;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Select;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;

class SelectionEtatForm extends Form {
    use EtatTypeServiceAwareTrait;

    public function init(): void
    {
        $this->add([
            'type' => Select::class,
            'name' => 'etat',
            'options' => [
                'label' => "État * :",
                'empty_option' => "Sélectionner un état ...",
                'value_options' => $this->getEtatTypeService()->getEtatTypesAsOption(),
            ],
            'attributes' => [
                'id' => 'etat',
                'class'             => 'bootstrap-selectpicker show-tick',
                'data-live-search'  => 'true',
            ],
        ]);
        //submit
        $this->add([
            'type' => Button::class,
            'name' => 'bouton',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);

        $this->setInputFilter((new Factory())->createInputFilter([
            'etat' => [
                'required' => true,
            ],
        ]));
    }

    public function reinit(string $categorieCode): void
    {
        $etatTypes = $this->getEtatTypeService()->getEtatsTypesByCategorieCode($categorieCode);
        $options = [];
        foreach ($etatTypes as $etatType) {
            $options[$etatType->getId()] = $this->getEtatTypeService()->optionify($etatType);
        }
        /** @var Select $select */
        $select = $this->get('etat');
        $select->setValueOptions($options);
    }
}