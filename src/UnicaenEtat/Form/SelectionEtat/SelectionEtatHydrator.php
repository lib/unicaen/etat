<?php

namespace UnicaenEtat\Form\SelectionEtat;

use UnicaenEtat\Entity\Db\HasEtatInterface;
use Laminas\Hydrator\HydratorInterface;
use UnicaenEtat\Entity\Db\HasEtatsInterface;
use UnicaenEtat\Service\EtatInstance\EtatInstanceServiceAwareTrait;
use UnicaenEtat\Service\EtatType\EtatTypeServiceAwareTrait;

class SelectionEtatHydrator implements HydratorInterface {
    use EtatTypeServiceAwareTrait;
    use EtatInstanceServiceAwareTrait;

    private ?string $categorie = null;
    public function setCategorie(?string $categorie): void
    {
        $this->categorie = $categorie;
    }

    /**
     * @param HasEtatInterface|HasEtatsInterface $object
     * @return array
     */
    public function extract(object $object) : array
    {
        $etat = null;
        if ($object instanceof HasEtatInterface) $etat = $object->getEtat();
        if ($object instanceof HasEtatsInterface) $etat = $object->getEtatActif();

        $data = [
            'etat' => ($etat)?$etat->getType()->getId():null,
        ];
        return $data;
    }

    /**
     * @param array $data
     * @param HasEtatInterface|HasEtatsInterface $object
     * @return HasEtatInterface
     */
    public function hydrate(array $data, object $object): object
    {
        $etat = (isset($data['etat']))?$this->getEtatTypeService()->getEtatType($data['etat']):null;
        if ($etat) $this->getEtatInstanceService()->setEtatActif($object, $etat->getCode(), $this->categorie);
        return $object;
    }

}