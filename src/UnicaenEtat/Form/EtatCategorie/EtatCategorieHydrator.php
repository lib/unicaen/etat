<?php

namespace UnicaenEtat\Form\EtatCategorie;

use UnicaenEtat\Entity\Db\EtatCategorie;
use Laminas\Hydrator\HydratorInterface;

class EtatCategorieHydrator implements HydratorInterface
{
    /**
     * @param EtatCategorie $object
     * @return array
     */
    public function extract(object $object) : array
    {
        $data = [
            'code' => $object->getCode(),
            'libelle' => $object->getLibelle(),
            'couleur' => $object->getCouleur(),
            'icone' => $object->getIcone(),
            'ordre' => $object->getOrdre(),
        ];

        return $data;
    }

    /**
     * @param array $data
     * @param EtatCategorie $object
     * @return EtatCategorie
     */
    public function hydrate(array $data, object $object)
    {
        $code = (isset($data['code']) AND trim($data['code']) !== "")?trim($data['code']):null;
        $libelle = (isset($data['libelle']) AND trim($data['libelle']) !== "")?trim($data['libelle']):null;
        $couleur = (isset($data['couleur']) AND trim($data['couleur']) !== "")?trim($data['couleur']):null;
        $icone = (isset($data['icone']) AND trim($data['icone']) !== "")?trim($data['icone']):null;
        $ordre = (isset($data['ordre']) AND trim($data['ordre']) !== "")?trim($data['ordre']):null;

        $object->setCode($code);
        $object->setLibelle($libelle);
        $object->setCouleur($couleur);
        $object->setIcone($icone);
        $object->setOrdre($ordre);
        return $object;
    }

}