<?php

namespace UnicaenEtat\Form\EtatCategorie;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class EtatCategorieFormFactory {

    /**
     * @param ContainerInterface $container
     * @return EtatCategorieForm
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : EtatCategorieForm
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $hydrator = $container->get('HydratorManager')->get(EtatCategorieHydrator::class);

        $form = new EtatCategorieForm();
        $form->setEntityManager($entityManager);
        $form->setHydrator($hydrator);
        return $form;
    }
}