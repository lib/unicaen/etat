<?php

namespace UnicaenEtat\Form\EtatCategorie;

trait EtatCategorieFormAwareTrait {

    private EtatCategorieForm $etatCategorieForm;

    /**
     * @return EtatCategorieForm
     */
    public function getEtatCategorieForm(): EtatCategorieForm
    {
        return $this->etatCategorieForm;
    }

    /**
     * @param EtatCategorieForm $etatCategorieForm
     * @return EtatCategorieForm
     */
    public function setEtatCategorieForm(EtatCategorieForm $etatCategorieForm): EtatCategorieForm
    {
        $this->etatCategorieForm = $etatCategorieForm;
        return $this->etatCategorieForm;
    }
}