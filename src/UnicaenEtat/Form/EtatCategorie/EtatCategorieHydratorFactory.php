<?php

namespace UnicaenEtat\Form\EtatCategorie;

use Interop\Container\ContainerInterface;

class EtatCategorieHydratorFactory {

    /**
     * @param ContainerInterface $container
     * @return EtatCategorieHydrator
     */
    public function __invoke(ContainerInterface $container) : EtatCategorieHydrator
    {
        $hydrator = new EtatCategorieHydrator();
        return $hydrator;
    }
}