<?php

namespace UnicaenEtat\Form\EtatType;

use Interop\Container\ContainerInterface;
use UnicaenEtat\Service\EtatCategorie\EtatCategorieService;
use UnicaenEtat\Service\EtatType\EtatTypeService;

class EtatTypeHydratorFactory {

    /**
     * @param ContainerInterface $container
     * @return EtatTypeHydrator
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : EtatTypeHydrator
    {
        /**
         * @var EtatCategorieService $etatCategorieService
         */
        $etatCategorieService = $container->get(EtatCategorieService::class);

        $hydrator = new EtatTypeHydrator();
        $hydrator->setEtatCategorieService($etatCategorieService);
        return $hydrator;
    }
}