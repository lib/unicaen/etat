<?php

namespace UnicaenEtat\Form\EtatType;

use Laminas\Hydrator\HydratorInterface;
use UnicaenEtat\Entity\Db\EtatType;
use UnicaenEtat\Service\EtatCategorie\EtatCategorieServiceAwareTrait;

class EtatTypeHydrator implements HydratorInterface {
    use EtatCategorieServiceAwareTrait;

    /**
     * @param EtatType $object
     * @return array
     */
    public function extract(object $object) : array
    {
        $data = [
            'code' => ($object)?$object->getCode():null,
            'libelle' => ($object)?$object->getLibelle():null,
            'categorie' => ($object  AND $object->getCategorie())?$object->getCategorie()->getCode():null,
            'icone' => ($object)?$object->getIcone():null,
            'couleur' => ($object)?$object->getCouleur():null,
            'ordre' => ($object)?$object->getOrdre():9999,
        ];
        return $data;
    }

    /**
     * @param array $data
     * @param EtatType $object
     * @return EtatType
     */
    public function hydrate(array $data, object $object)
    {
        $code = (isset($data['code']) AND trim($data['code']) !== "")?trim($data['code']):null;
        $libelle = (isset($data['libelle']) AND trim($data['libelle']) !== "")?trim($data['libelle']):null;
        $icone = (isset($data['icone']) AND trim($data['icone']) !== "")?trim($data['icone']):null;
        $couleur = (isset($data['couleur']) AND trim($data['couleur']) !== "")?trim($data['couleur']):null;
        $categorie = isset($data['categorie'])?$this->getEtatCategorieService()->getEtatCategorieByCode($data['categorie']):null;
        $ordre = (isset($data['ordre']) AND trim($data['ordre']) !== "")?trim($data['ordre']):9999;

        $object->setCode($code);
        $object->setLibelle($libelle);
        $object->setIcone($icone);
        $object->setCouleur($couleur);
        $object->setCategorie($categorie);
        $object->setOrdre($ordre);

        return $object;
    }

}