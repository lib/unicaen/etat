<?php

namespace UnicaenEtat\Form\EtatType;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEtat\Service\EtatCategorie\EtatCategorieService;
use UnicaenEtat\Service\EtatType\EtatTypeService;

class EtatTypeFormFactory {

    /**
     * @param ContainerInterface $container
     * @return EtatTypeForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : EtatTypeForm
    {
        /**
         * @var EntityManager $entityManager
         * @var EtatCategorieService $etatCategorieService
         * @var EtatTypeService $etatTypeService
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $etatCategorieService = $container->get(EtatCategorieService::class);
        $etatTypeService = $container->get(EtatTypeService::class);

        /** @var EtatTypeHydrator $hydrator */
        $hydrator = $container->get('HydratorManager')->get(EtatTypeHydrator::class);

        $form = new EtatTypeForm();
        $form->setEntityManager($entityManager);
        $form->setEtatCategorieService($etatCategorieService);
        $form->setEtatTypeService($etatTypeService);
        $form->setHydrator($hydrator);

        return $form;
    }
}