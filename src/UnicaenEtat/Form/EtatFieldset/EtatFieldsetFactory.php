<?php

namespace UnicaenEtat\Form\EtatFieldset;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEtat\Service\EtatType\EtatTypeService;

class EtatFieldsetFactory {

    /**
     * @param ContainerInterface $container
     * @return EtatFieldset
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): EtatFieldset
    {
        /**
         * @var EtatTypeService $etatTypeService
         */
        $etatTypeService = $container->get(EtatTypeService::class);

        $fieldset = new EtatFieldset();
        $fieldset->setEtatTypeService($etatTypeService);
        return $fieldset;
    }
}