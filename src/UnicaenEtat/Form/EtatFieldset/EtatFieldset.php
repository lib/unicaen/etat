<?php

namespace UnicaenEtat\Form\EtatFieldset;

use Laminas\Form\Element\Select;
use Laminas\Form\Fieldset;
use UnicaenEtat\Entity\Db\EtatType;
use UnicaenEtat\Service\EtatType\EtatTypeServiceAwareTrait;

class EtatFieldset extends Fieldset
{
    use EtatTypeServiceAwareTrait;

    public function init()
    {
        $this->add([
            'type' => Select::class,
            'name' => 'etat',
            'options' => [
                'label' => "État * :",
                'empty_option' => "Sélectionner un état ...",
                'value_options' => $this->getEtatTypeService()->getEtatTypesAsOption(),
            ],
            'attributes' => [
                'id' => 'etat',
                'class'             => 'bootstrap-selectpicker show-tick',
                'data-live-search'  => 'true',
            ],
        ]);
    }

    /**
     * @param EtatType[] $etats
     */
    public function resetEtats(array $etats): array
    {
        $options = [];
        /** @var EtatType $etat */
        foreach ($etats as $etat) {
            $options[$etat->getId()] = $this->getEtatTypeService()->optionify($etat);
        }

        /** @var Select $select */
        $select = $this->get('etat');
        $select->setValueOptions($options);
    }
}