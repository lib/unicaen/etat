<?php

namespace UnicaenEtat\Controller;

use Interop\Container\ContainerInterface;
use UnicaenEtat\Form\EtatCategorie\EtatCategorieForm;
use UnicaenEtat\Service\EtatCategorie\EtatCategorieService;

class EtatCategorieControllerFactory {

    /**
     * @param ContainerInterface $container
     * @return EtatCategorieController
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : EtatCategorieController
    {
        /**
         * @var EtatCategorieService $etatTypeService
         */
        $etatCategorieService = $container->get(EtatCategorieService::class);

        /**
         * @var EtatCategorieForm $etatTypeForm
         */
        $etatTypeForm = $container->get('FormElementManager')->get(EtatCategorieForm::class);

        $controller = new EtatCategorieController();
        $controller->setEtatCategorieService($etatCategorieService);
        $controller->setEtatCategorieForm($etatTypeForm);
        return $controller;
    }

}