<?php

namespace UnicaenEtat\Controller;

use Interop\Container\ContainerInterface;
use UnicaenEtat\Form\EtatType\EtatTypeForm;
use UnicaenEtat\Service\EtatCategorie\EtatCategorieService;
use UnicaenEtat\Service\EtatType\EtatTypeService;

class EtatTypeControllerFactory {

    /**
     * @param ContainerInterface $container
     * @return EtatTypeController
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : EtatTypeController
    {
        /**
         * @var EtatCategorieService $etatCategorieService
         * @var EtatTypeService $etatTypeService
         */
        $etatCategorieService = $container->get(EtatCategorieService::class);
        $etatTypeService = $container->get(EtatTypeService::class);

        /**
         * @var EtatTypeForm $etatTypeForm
         */
        $etatTypeForm = $container->get('FormElementManager')->get(EtatTypeForm::class);

        $controller = new EtatTypeController();
        $controller->setEtatCategorieService($etatCategorieService);
        $controller->setEtatTypeService($etatTypeService);
        $controller->setEtatTypeForm($etatTypeForm);
        return $controller;
    }

}