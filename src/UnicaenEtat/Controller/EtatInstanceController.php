<?php

namespace UnicaenEtat\Controller;

use Laminas\Http\Request;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;
use UnicaenEtat\Service\EtatCategorie\EtatCategorieServiceAwareTrait;
use UnicaenEtat\Service\EtatInstance\EtatInstanceServiceAwareTrait;
use UnicaenEtat\Service\EtatType\EtatTypeServiceAwareTrait;
use UnicaenUtilisateur\Entity\Db\User;

class EtatInstanceController extends AbstractActionController {
    use EtatInstanceServiceAwareTrait;
    use EtatCategorieServiceAwareTrait;
    use EtatTypeServiceAwareTrait;

    public function indexAction() : ViewModel
    {
        $params = $this->params()->fromQuery();
        $instances = $this->getEtatInstanceService()->getEtatsInstancesWithFiltre($params);
        $categories = $this->getEtatCategorieService()->getEtatsCategories();
        $types = $this->getEtatTypeService()->getEtatsTypes();

        return new ViewModel([
            'params' => $params,
            'instances' => $instances,
            'categories' => $categories,
            'types' => $types,
        ]);
    }

    public function supprimerAction()
    {
        $etat = $this->getEtatInstanceService()->getResquestedEtatInstance($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getEtatInstanceService()->delete($etat);
            exit();
        }

        $vm = new ViewModel();
        if ($etat !== null) {
            $vm->setTemplate('unicaen-etat/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression de l'instance d'état " . $etat->getId(),
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('unicaen-etat/etat-instance/supprimer', ["instance" => $etat->getId()], [], true),
            ]);
        }
        return $vm;
    }
    public function rechercherUtilisateurAction()
    {
        if (($term = $this->params()->fromQuery('term'))) {
            /** @var User $utilisateurs */
            $utilisateurs = $this->getEtatInstanceService()->findUtilisateurByTerm($term);
            foreach ($utilisateurs as $utilisateur) {
                $result[] = array(
                    'id' => $utilisateur->getHistoCreateur()->getId(),
                    'label' => $utilisateur->getHistoCreateur()->getDisplayName(),
                );
            }
            usort($result, function ($a, $b) {
                return strcmp($a['label'], $b['label']);
            });
            return new JsonModel($result);
        }
        exit;
    }
}