<?php

namespace UnicaenEtat\Controller;

use Laminas\Http\Request;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenEtat\Entity\Db\EtatCategorie;
use UnicaenEtat\Form\EtatCategorie\EtatCategorieFormAwareTrait;
use UnicaenEtat\Service\EtatCategorie\EtatCategorieServiceAwareTrait;

class EtatCategorieController extends AbstractActionController
{
    use EtatCategorieServiceAwareTrait;
    use EtatCategorieFormAwareTrait;

    public function indexAction() : ViewModel
    {
        $etatsCategories = $this->getEtatCategorieService()->getEtatsCategories();
        return new ViewModel([
            'etatsCategories' => $etatsCategories,
        ]);
    }

    public function ajouterAction() : ViewModel
    {
        $etatCategorie = new EtatCategorie();

        $form = $this->getEtatCategorieForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-etat/etat-categorie/ajouter', [], [], true));
        $form->bind($etatCategorie);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getEtatCategorieService()->create($etatCategorie);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-etat/default/default-form');
        $vm->setVariables([
            'title' => "Ajout d'une catégorie d'état",
            'form' => $form,
        ]);
        return $vm;
    }

    public function modifierAction() : ViewModel
    {
        $etatCategorie = $this->getEtatCategorieService()->getRequestedEtatCategorie($this);

        $form = $this->getEtatCategorieForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-etat/etat-categorie/modifier', ['categorie' => $etatCategorie->getId()], [], true));
        $form->bind($etatCategorie);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getEtatCategorieService()->update($etatCategorie);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-etat/default/default-form');
        $vm->setVariables([
            'title' => "Modification d'une catégorie d'état",
            'form' => $form,
        ]);
        return $vm;
    }

    public function supprimerAction() : ViewModel
    {
        $etatCategorie = $this->getEtatCategorieService()->getRequestedEtatCategorie($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getEtatCategorieService()->delete($etatCategorie);
            exit();
        }

        $vm = new ViewModel();
        if ($etatCategorie !== null) {
            $vm->setTemplate('unicaen-etat/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression de la catégorie d'état " . $etatCategorie->getCode(),
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('unicaen-etat/etat-categorie/supprimer', ["categorie" => $etatCategorie->getId()], [], true),
            ]);
        }
        return $vm;
    }

}