<?php

namespace UnicaenEtat\Controller;

use Laminas\Http\Request;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenEtat\Entity\Db\EtatType;
use UnicaenEtat\Form\EtatType\EtatTypeFormAwareTrait;
use UnicaenEtat\Service\EtatCategorie\EtatCategorieServiceAwareTrait;
use UnicaenEtat\Service\EtatType\EtatTypeServiceAwareTrait;

class EtatTypeController extends AbstractActionController
{
    use EtatTypeServiceAwareTrait;
    use EtatCategorieServiceAwareTrait;
    use EtatTypeFormAwareTrait;

    public function indexAction() : ViewModel
    {
        $categories = $this->getEtatCategorieService()->getEtatsCategories();
        
        $dictionnaire = [];
        foreach ($categories as $categorie) {
            $types = $this->getEtatTypeService()->getEtatsTypesByCategorie($categorie);
            $dictionnaire[$categorie->getId()] = $types;
        }
        return new ViewModel([
            'categories' => $categories,
            'dictionnaire' => $dictionnaire,
        ]);
    }

    public function ajouterAction() : ViewModel
    {
        $etat = new EtatType();

        $form = $this->getEtatTypeForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-etat/etat-type/ajouter', [], [], true));
        $form->bind($etat);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getEtatTypeService()->create($etat);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-etat/default/default-form');
        $vm->setVariables([
            'title' => "Ajout d'un type d'état",
            'form' => $form,
        ]);
        return $vm;
    }

    public function modifierAction() : ViewModel
    {
        $type = $this->getEtatTypeService()->getRequestedEtatType($this);

        $form = $this->getEtatTypeForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-etat/etat-type/modifier', ['type' => $type->getId()], [], true));
        $form->bind($type);
        $form->get('old-code')->setValue($type->getCode());

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getEtatTypeService()->update($type);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('unicaen-etat/default/default-form');
        $vm->setVariables([
            'title' => "Modification d'un type d'état",
            'form' => $form,
        ]);
        return $vm;
    }

    public function supprimerAction()
    {
        $etat = $this->getEtatTypeService()->getRequestedEtatType($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getEtatTypeService()->delete($etat);
            exit();
        }

        $vm = new ViewModel();
        if ($etat !== null) {
            $vm->setTemplate('unicaen-etat/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression du type d'état " . $etat->getCode(),
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('unicaen-etat/etat-type/supprimer', ["type" => $etat->getId()], [], true),
            ]);
        }
        return $vm;
    }

}