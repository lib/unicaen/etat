<?php

namespace UnicaenEtat\Controller;

use Interop\Container\ContainerInterface;
use UnicaenEtat\Service\EtatCategorie\EtatCategorieService;
use UnicaenEtat\Service\EtatInstance\EtatInstanceService;
use UnicaenEtat\Service\EtatType\EtatTypeService;

class EtatInstanceControllerFactory {

    /**
     * @param ContainerInterface $container
     * @return EtatInstanceController
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : EtatInstanceController
    {
        /**
         * @var EtatInstanceService $instanceService
         * @var EtatCategorieService $etatTypeService
         * @var EtatTypeService $typeService
         */
        $instanceService = $container->get(EtatInstanceService::class);
        $etatCategorieService = $container->get(EtatCategorieService::class);
        $typeService = $container->get(EtatTypeService::class);

        $controller = new EtatInstanceController();
        $controller->setEtatInstanceService($instanceService);
        $controller->setEtatCategorieService($etatCategorieService);
        $controller->setEtatTypeService($typeService);
        return $controller;
    }
}