<?php

namespace UnicaenEtat\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class EtatPrivileges extends Privileges
{
    const ETAT_INDEX = 'etat-etat_index';
    const ETAT_AJOUTER = 'etat-etat_ajouter';
    const ETAT_MODIFIER = 'etat-etat_modifier';
    const ETAT_HISTORISER = 'etat-etat_historiser';
    const ETAT_DETRUIRE = 'etat-etat_detruire';
}