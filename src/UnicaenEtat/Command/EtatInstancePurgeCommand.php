<?php

namespace UnicaenEtat\Command;

use DateInterval;
use DateTime;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use UnicaenEtat\Entity\Db\EtatInstance;
use UnicaenEtat\Service\EtatInstance\EtatInstanceServiceAwareTrait;

/***
 * @desc Supprime toutes les instances d'état qui sont archivées depuis un certains temps
 * paramétre à définir dans config unicaen-etat > conservation-time sour la forme d'un DateInterval
 * Par défaut : 1 an
 * TODO : permettre dans la conf de spécifier les types et catégories d'états qui doivent être purgée
 * TODO : gérer des dates différents par état/types
 * TODO : rendre des types/catégories non purgeable
 */
class EtatInstancePurgeCommand extends Command
{
    protected static $defaultName = 'etatInstance:purge';

    protected ?DateInterval $conservationTime = null;
    public function setConservationTime(DateInterval $conservationTime): static
    {
        $this->conservationTime = $conservationTime;
        return $this;
    }
    public function getDateSuppression() : DateTime
    {
        $date = new DateTime();
        $conservationTime = ($this->conservationTime) ?? new DateInterval('P1Y');
        $date->sub($conservationTime);
        return $date;
    }
    protected ?string $codeType = null;
    protected ?string $codeCategorie = null;

    public function getCodeType(): ?string
    {
        return $this->codeType;
    }

    public function getCodeCategorie(): ?string
    {
        return $this->codeCategorie;
    }

    public function setCodeType(?string $codeType): static
    {
        $this->codeType = $codeType;
        return $this;
    }
    public function setCodeCategorie(?string $codeCategorie): static
    {
        $this->codeCategorie = $codeCategorie;
        return $this;
    }


    use EtatInstanceServiceAwareTrait;

    protected function configure() : static
    {
        $this->setDescription("Supprime les instances d'états archivés");
        $this->addOption('time', '-t', InputOption::VALUE_OPTIONAL, "Intervale de temps que l'on souhaite conserver -- format : P1Y");
        $this->addOption('categorie', '-c', InputOption::VALUE_OPTIONAL, "code de la catégorie d'état que l'on souhaite purger");
        $this->addOption('type', '-type', InputOption::VALUE_OPTIONAL, "code du type d'état que l'on souhaite purger");
        return $this;
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);
        try{
            $time = $input->getOption('time');
            if(isset($time) && $time != ""){
                $this->conservationTime = new DateInterval($time);
            }
            $codeCategorie = $input->getOption('categorie');
            if(isset($codeCategorie) && $codeCategorie != ""){
                $this->codeCategorie = $codeCategorie;
            }
            $codeType = $input->getOption('type');
            if(isset($codeType) && $codeType != ""){
                $this->codeType = $codeType;
            }
        }
        catch (Exception $e){
            $io->error($e->getMessage());
            exit(-1);
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $io->title("Suppression des instances d'états archivées");

            $date = $this->getDateSuppression();
            $codeCategorie = $this->getCodeCategorie();
            $codeType = $this->getCodeType();

            $qb =$this->getEtatInstanceService()->createQueryBuilder();
            $qb->andWhere("einstance.histoDestruction < :date");
            $qb->setParameter("date", $date);
            if(isset($codeCategorie) && $codeCategorie != ""){
                $qb->andWhere($qb->expr()->eq('ecategorie.code',':codeCategorie'));
                $qb->setParameter("codeCategorie", $codeCategorie);
            };
            if(isset($codeType) && $codeType != ""){
                $qb->andWhere($qb->expr()->eq('etype.code',':codeType'));
                $qb->setParameter("codeType", $codeType);
            };
            $etatsInstances = $qb->getQuery()->getResult();

            if(!empty($etatsInstances)) {
                $io->progressStart(sizeof($etatsInstances));
                /** @var EtatInstance $log */
                foreach ($etatsInstances as $etatInstance) {
                    $this->getEtatInstanceService()->delete($etatInstance);
                    $io->progressAdvance();
                }
                $io->progressFinish();
            }
            else{
                $io->text("Aucune instance d'états à supprimer");
            }
            $io->success("Suppression terminée");
        }
        catch (Exception $e){
                $io->error($e->getMessage());
                return self::FAILURE;
        }
        return self::SUCCESS;
    }

}