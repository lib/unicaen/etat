<?php

namespace UnicaenEtat\Command;

use DateInterval;
use Exception;
use Psr\Container\ContainerInterface;
use UnicaenEtat\Service\EtatInstance\EtatInstanceService;

class EtatInstancePurgeCommandFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \Exception
     */
    public function __invoke(ContainerInterface $container): EtatInstancePurgeCommand
    {
        $command = new EtatInstancePurgeCommand();
        $command->setEtatInstanceService($container->get(EtatInstanceService::class));

        $config = $container->get('Configuration');
        if (isset($config['unicaen-etat']['conservation-time'])) {
            $conservationTime = $config['unicaen-etat']['conservation-time'];
            if(!$conservationTime instanceof DateInterval) {
                throw new Exception("Le paramètre de configuration 'unicaen-etat > conservation-time' doit être un DateInterval");
            }
            $command->setConservationTime($conservationTime);
        }

        return $command;
    }
}