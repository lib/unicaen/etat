<?php

namespace UnicaenEtat\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;
use UnicaenEtat\Entity\Db\EtatInstance;

class EtatInstanceViewHelper extends AbstractHelper
{
    /**
     * @param EtatInstance $instance
     * @param array $options
     * @return string|Partial
     * 
     * -- OPTIONS --
     * 'display-date' : affiche de la date du passage à l'état | default:true;
     * 'display-user' : affiche de qui a modifier en dernier l'état | default:true;
     *  Les autres options sont celles de EtatTypeViewHelper si les deux premiéres options sont à false
     * 'display-categorie' : affiche la partie catégorie | default:true;
     * 'display-type' : affiche la partie type  | default:true;
     * 'unknown-icon' : icone si non définie | défault: fas fa-question;
     * 'unknown-color' : couleur si non définie | défault: gray;
     * 'unknown-type-libelle' : Libelle du type si l'instance n'existe pas | défault: 'Aucun état';
     * 'unknown-categorie-libelle' : Libelle de la catégorie ssi l'instance n'existe pas | défault: 'Aucun état';
     */
    public function __invoke(?EtatInstance $instance, array $options = [])
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('etat-instance', ['instance' => $instance, 'options' => $options]);
    }
}