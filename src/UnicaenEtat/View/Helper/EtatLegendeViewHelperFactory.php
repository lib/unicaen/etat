<?php

namespace UnicaenEtat\View\Helper;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEtat\Service\EtatCategorie\EtatCategorieService;

class EtatLegendeViewHelperFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): EtatLegendeViewHelper
    {
        /**
         * @var EtatCategorieService $etatCategorieService
         */
        $etatCategorieService = $container->get(EtatCategorieService::class);

        $helper = new EtatLegendeViewHelper();
        $helper->setEtatCategorieService($etatCategorieService);
        return $helper;
    }
}