<?php

namespace UnicaenEtat\View\Helper;

use Laminas\View\Renderer\PhpRenderer;
use UnicaenEtat\Entity\Db\EtatCategorie;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Resolver\TemplatePathStack;

class EtatCategorieViewHelper extends AbstractHelper
{
    /**
     *  -- OPTIONS --
     *  'display-libelle' : affiche de le libelle de la catégorie | default: false;
     * @param EtatCategorie $etatCategorie
     * @param array $options
     * @return string|Partial
     */
    public function __invoke(EtatCategorie $etatCategorie, array $options = []): string|Partial
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('etat-categorie', ['etatCategorie' => $etatCategorie, 'options' => $options]);
    }
}