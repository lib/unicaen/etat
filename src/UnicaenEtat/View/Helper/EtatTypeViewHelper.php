<?php

namespace UnicaenEtat\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;
use UnicaenEtat\Entity\Db\EtatInstance;
use UnicaenEtat\Entity\Db\EtatType;

class EtatTypeViewHelper extends AbstractHelper
{
    /**
     * @param EtatType $type
     * @param array $options
     * @return string|Partial
     * 
     * -- OPTIONS --
     * 'display-categorie' : affiche la partie catégorie | default:true;
     * 'display-type' : affiche la partie type  | default:true;
     * 'display-libelle' : affiche le libelle du type  | default:false;
     * 'unknown-icon' : icone si non définie | défault: fas fa-question;
     * 'unknown-color' : couleur si non définie | défault: gray;
     * 'unknown-type-libelle' : Libelle du type si le type n'existe pas | défault: 'Aucun état';
     * 'unknown-categorie-libelle' : Libelle de la catégorie si le type n'existe pas | défault: 'Aucun état';
 */
    public function __invoke(?EtatType $type, array $options = [])
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('etat-type', ['type' => $type, 'options' => $options]);
    }
}