<?php

namespace UnicaenEtat\View\Helper;

use Laminas\View\Renderer\PhpRenderer;
use RuntimeException;
use UnicaenEtat\Entity\Db\EtatCategorie;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Resolver\TemplatePathStack;
use UnicaenEtat\Service\EtatCategorie\EtatCategorieServiceAwareTrait;

class EtatLegendeViewHelper extends AbstractHelper
{
    use EtatCategorieServiceAwareTrait;

    /**
     * TODO :: par code aussi
     *
     * @param EtatCategorie|string $etatCategorie
     * @param array $options
     * @return string|Partial
     */
    public function __invoke(EtatCategorie|string $etatCategorie, array $options = []): Partial|string
    {
        if (is_string($etatCategorie)) {
            $code = $etatCategorie;
            $etatCategorie = $this->getEtatCategorieService()->getEtatCategorieByCode($code);
            if ($etatCategorie === null) throw new RuntimeException("Aucune catégorie d'état ne correspond au code [".$code."]");
        }

        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('etat-legende', ['etatCategorie' => $etatCategorie, 'options' => $options]);
    }
}