<?php

namespace UnicaenEtat\Entity\Db;

use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;

interface HasEtatsInterface {

    public function getEtats(): ?Collection;
    public function getEtatsByCode(string $code, bool $withHisto) : ?Collection;
    public function getEtatActif(): ?EtatInstance;
    public function isEtatActif(string $code, ?DateTime $date = null) : bool;
    public function addEtat(EtatInstance $etat): void;
    public function removeEtat(EtatInstance $etat): void;
    public function clearEtats(): void;
    static public function decorateWithEtats(QueryBuilder $qb, string $entityName,  array $etats = []) : QueryBuilder;
    static public function decorateWithEtatsCodes(QueryBuilder $qb, string $entityName,  array $etats = []) : QueryBuilder;

}