<?php

namespace UnicaenEtat\Entity\Db;

interface HasEtatInterface {

    public function getEtat(): ?EtatInstance;
    public function setEtat(?EtatInstance $etat): void;
}