<?php

namespace UnicaenEtat\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use UnicaenApp\Entity\HistoriqueAwareInterface;
use UnicaenApp\Entity\HistoriqueAwareTrait;

class EtatType {

    private int $id;
    private ?string $code = null;
    private ?EtatCategorie $categorie = null;
    private ?string $libelle = null;
    private ?string $icone = null;
    private ?string $couleur = null;
    private ?int $ordre = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return EtatCategorie|null
     */
    public function getCategorie(): ?EtatCategorie
    {
        return $this->categorie;
    }

    /**
     * @param EtatCategorie|null $categorie
     */
    public function setCategorie(?EtatCategorie $categorie): void
    {
        $this->categorie = $categorie;
    }

    /**
     * @return string|null
     */
    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    /**
     * @param string $libelle
     */
    public function setLibelle(string $libelle): void
    {
        $this->libelle = $libelle;
    }

    /**
     * @return string|null
     */
    public function getIcone(): ?string
    {
        return $this->icone;
    }

    /**
     * @param string|null $icone
     */
    public function setIcone(?string $icone): void
    {
        $this->icone = $icone;
    }

    /**
     * @return string|null
     */
    public function getCouleur(): ?string
    {
        return $this->couleur;
    }

    /**
     * @param string|null $couleur
     */
    public function setCouleur(?string $couleur): void
    {
        $this->couleur = $couleur;
    }

    /**
     * @return int|null
     */
    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    /**
     * @param int $ordre
     */
    public function setOrdre(int $ordre): void
    {
        $this->ordre = $ordre;
    }

}