<?php

namespace UnicaenEtat\Entity\Db;

use Doctrine\ORM\QueryBuilder;

trait HasEtatTrait {

    private ?EtatInstance $etat=null;

    public function getEtat(): ?EtatInstance
    {
        return $this->etat;
    }

    public function setEtat(?EtatInstance $etat): void
    {
        $this->etat = $etat;
    }

    /**
     * @param string $etatCode
     * @return boolean
     */
    public function isEtat(string $etatCode) : bool
    {
        if ($this->etat === null) return false;
        return ($this->etat->getType()->getCode() === $etatCode);
    }

    /** Decorateur ****************************************************************************************************/

    /**
     * @param QueryBuilder $qb
     * @param string $entityName
     * @param EtatType|null $etat
     * @return QueryBuilder
     */
    static public function decorateWithEtat(QueryBuilder $qb, string $entityName,  ?EtatType $etat = null) : QueryBuilder
    {
        $qb = $qb
            ->leftJoin($entityName . '.etat', 'etat')->addSelect('etat')
            ->leftJoin('etat.type', 'type')->addSelect('type')
        ;

        if ($etat !== null) {
            $qb = $qb->andWhere($entityName . '.etat = :etat')
                    ->setParameter('etat', $etat);
        }

        return $qb;
    }
    /**
     * @param QueryBuilder $qb
     * @param string $entityName
     * @param EtatType[] $etats
     * @return QueryBuilder
     */
    static public function decorateWithEtats(QueryBuilder $qb, string $entityName,  array $etats = []) : QueryBuilder
    {
        $qb = $qb
            ->leftJoin($entityName . '.etat', 'etat')->addSelect('etat')
            ->leftJoin('etat.type', 'type')->addSelect('type')
        ;

        if (!empty($etats) !== null) {
            $qb = $qb->andWhere($entityName . '.etat in (:etats)')
                ->setParameter('etats', $etats);
        }
        return $qb;
    }

}