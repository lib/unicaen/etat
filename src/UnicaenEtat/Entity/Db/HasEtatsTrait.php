<?php

namespace UnicaenEtat\Entity\Db;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;

trait HasEtatsTrait {

    private ?Collection $etats = null;

    public function getEtats() : ?Collection
    {
        return $this->etats;
    }
    public function getEtatsByCode(string $code, bool $withHisto) : ?Collection
    {
        $etats_ = $this->etats;
        $etats = new ArrayCollection();

        /** @var EtatInstance $etat */
        foreach ($etats_ as $etat)
        {
            if ($etat->getType()->getCode() === $code) {
                if ($withHisto || $etat->estNonHistorise()) $etats->add($etat);
            }
        }

        return $etats;
    }

    public function getEtatActif(?DateTime $date = null) : ?EtatInstance
    {
        if ($this->etats === null) return null;
        if ($date === null) $date = new DateTime();

        $etat = null;

        /** @var EtatInstance $etat_ */
        foreach ($this->etats as $etat_) {
            if ($etat_->estNonHistorise($date)) {
                if ($etat === null || $etat_->getHistoCreation() > $etat->getHistoCreation()) $etat = $etat_;
            }
        }
        return $etat;
    }

    public function isEtatActif(string $code, ?DateTime $date = null) : bool
    {
        $etatActif = $this->getEtatActif($date);
        return ($etatActif && $etatActif->getType()->getCode() === $code);
    }

    public function addEtat(EtatInstance $etat): void
    {
        $this->etats->add($etat);
    }

    public function removeEtat(EtatInstance $etat): void
    {
        $this->etats->removeElement($etat);
    }

    public function clearEtats() : void
    {
        $this->etats->clear();
    }

    static public function decorateWithEtats(QueryBuilder $qb, string $entityName,  array $etats = []) : QueryBuilder
    {
        $qb = $qb
            ->leftJoin($entityName . '.etats', 'etat')->addSelect('etat')
            ->leftJoin('etat.type', 'type')->addSelect('type')
            ->andWhere('etat.histoDestruction IS NULL')
        ;

        if (!empty($etats)) {
            $qb = $qb->andWhere('etat.type in (:etats)')
                ->setParameter('etats', $etats);
        }
        return $qb;
    }

    static public function decorateWithEtatsCodes(QueryBuilder $qb, string $entityName,  array $etats = [], string $paramsName = 'etat_') : QueryBuilder
    {
        $qb = $qb
            ->leftJoin($entityName . '.etats', $paramsName.'decorateurEtat')->addSelect($paramsName.'decorateurEtat')
            ->leftJoin($paramsName.'decorateurEtat.type', $paramsName.'decorateurType')->addSelect($paramsName.'decorateurType')
            ->andWhere($paramsName.'decorateurEtat.histoDestruction IS NULL')
        ;

        if (!empty($etats)) {
            $qb = $qb->andWhere($paramsName.'decorateurType.code in (:'.$paramsName.'etats)')
                ->setParameter($paramsName.'etats', $etats);
        }
        return $qb;
    }
}