<?php

namespace UnicaenEtat\Entity\Db;

use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;

class EtatInstance implements HistoriqueAwareInterface {
    use HistoriqueAwareTrait;

    private ?int $id = null;
    private ?EtatType $type = null;
    private ?string $infos =null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?EtatType
    {
        return $this->type;
    }

    public function setType(?EtatType $type): void
    {
        $this->type = $type;
    }

    public function getInfos(): ?string
    {
        return $this->infos;
    }

    public function setInfos(?string $infos): void
    {
        $this->infos = $infos;
    }

    public function getTypeLibelle() : string
    {
        return (isset($this->type)) ? $this->type->getLibelle() : "Aucun type";
    }

}