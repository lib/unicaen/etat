<?php

namespace UnicaenEtat;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenEtat\Controller\EtatTypeController;
use UnicaenEtat\Controller\EtatTypeControllerFactory;
use UnicaenEtat\Form\EtatFieldset\EtatFieldset;
use UnicaenEtat\Form\EtatFieldset\EtatFieldsetFactory;
use UnicaenEtat\Form\EtatType\EtatTypeForm;
use UnicaenEtat\Form\EtatType\EtatTypeFormFactory;
use UnicaenEtat\Form\EtatType\EtatTypeHydrator;
use UnicaenEtat\Form\EtatType\EtatTypeHydratorFactory;
use UnicaenEtat\Form\SelectionEtat\SelectionEtatForm;
use UnicaenEtat\Form\SelectionEtat\SelectionEtatFormFactory;
use UnicaenEtat\Form\SelectionEtat\SelectionEtatHydrator;
use UnicaenEtat\Form\SelectionEtat\SelectionEtatHydratorFactory;
use UnicaenEtat\Provider\Privilege\EtatPrivileges;
use UnicaenEtat\Service\EtatType\EtatTypeService;
use UnicaenEtat\Service\EtatType\EtatTypeServiceFactory;
use UnicaenEtat\View\Helper\EtatBadgeViewHelper;
use UnicaenEtat\View\Helper\EtatBadgeViewHelperFactory;
use UnicaenEtat\View\Helper\EtatTypeViewHelper;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => EtatTypeController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        EtatPrivileges::ETAT_INDEX,
                    ],
                ],
                [
                    'controller' => EtatTypeController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        EtatPrivileges::ETAT_AJOUTER,
                    ],
                ],
                [
                    'controller' => EtatTypeController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        EtatPrivileges::ETAT_MODIFIER,
                    ],
                ],
                [
                    'controller' => EtatTypeController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        EtatPrivileges::ETAT_DETRUIRE,
                    ],
                ],
            ],
        ],
    ],



    'router' => [
        'routes' => [
            'unicaen-etat' => [
                'child_routes' => [
                    'etat-type' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/etat-type',
                            'defaults' => [
                                'controller' => EtatTypeController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'ajouter' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/ajouter',
                                    'defaults' => [
                                        'controller' => EtatTypeController::class,
                                        'action' => 'ajouter',
                                    ],
                                ],
                                'may_terminate' => true,
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:type',
                                    'defaults' => [
                                        'controller' => EtatTypeController::class,
                                        'action' => 'modifier',
                                    ],
                                ],
                                'may_terminate' => true,
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:type',
                                    'defaults' => [
                                        'controller' => EtatTypeController::class,
                                        'action' => 'supprimer',
                                    ],
                                ],
                                'may_terminate' => true,
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            EtatTypeService::class => EtatTypeServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            EtatTypeController::class => EtatTypeControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            EtatTypeForm::class => EtatTypeFormFactory::class,
            EtatFieldset::class => EtatFieldsetFactory::class,
            SelectionEtatForm::class => SelectionEtatFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => array(
            EtatTypeHydrator::class => EtatTypeHydratorFactory::class,
            SelectionEtatHydrator::class => SelectionEtatHydratorFactory::class,
        ),
    ],
    'view_helpers' => [
        'invokables' => [
            'etattype' => EtatTypeViewHelper::class,
        ]
    ],

];