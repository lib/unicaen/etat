<?php

namespace UnicaenEtat;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenEtat\Controller\EtatCategorieController;
use UnicaenEtat\Controller\EtatCategorieControllerFactory;
use UnicaenEtat\Form\EtatCategorie\EtatCategorieForm;
use UnicaenEtat\Form\EtatCategorie\EtatCategorieFormFactory;
use UnicaenEtat\Form\EtatCategorie\EtatCategorieHydrator;
use UnicaenEtat\Form\EtatCategorie\EtatCategorieHydratorFactory;
use UnicaenEtat\Provider\Privilege\EtatPrivileges;
use UnicaenEtat\Service\EtatCategorie\EtatCategorieService;
use UnicaenEtat\Service\EtatCategorie\EtatCategorieServiceFactory;
use UnicaenEtat\View\Helper\EtatCategorieViewHelper;
use UnicaenEtat\View\Helper\EtatLegendeViewHelper;
use UnicaenEtat\View\Helper\EtatLegendeViewHelperFactory;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => EtatCategorieController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        EtatPrivileges::ETAT_INDEX,
                    ],
                ],
                [
                    'controller' => EtatCategorieController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        EtatPrivileges::ETAT_AJOUTER,
                    ],
                ],
                [
                    'controller' => EtatCategorieController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        EtatPrivileges::ETAT_MODIFIER,
                    ],
                ],
                [
                    'controller' => EtatCategorieController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        EtatPrivileges::ETAT_DETRUIRE,
                    ],
                ],
            ],
        ],
    ],

    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [
                    'administration' => [
//                        'pages' => [
//                            'unicaen-etat' => [
//                                'pages' => [
//                                    'etat-type' => [
//                                        'label' => 'État type',
//                                        'route' => 'unicaen-etat/etat-type',
//                                        'resource' => PrivilegeController::getResourceId(EtatCategorieController::class, 'index'),
//                                        'order'    => 10001,
//                                    ],
//                                ],
//                            ],
//                        ],
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-etat' => [
                'child_routes' => [
                    'etat-categorie' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/etat-categorie',
                            'defaults' => [
                                'controller' => EtatCategorieController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'ajouter' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/ajouter',
                                    'defaults' => [
                                        'controller' => EtatCategorieController::class,
                                        'action' => 'ajouter',
                                    ],
                                ],
                                'may_terminate' => true,
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:categorie',
                                    'defaults' => [
                                        'controller' => EtatCategorieController::class,
                                        'action' => 'modifier',
                                    ],
                                ],
                                'may_terminate' => true,
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:categorie',
                                    'defaults' => [
                                        'controller' => EtatCategorieController::class,
                                        'action' => 'supprimer',
                                    ],
                                ],
                                'may_terminate' => true,
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            EtatCategorieService::class => EtatCategorieServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            EtatCategorieController::class => EtatCategorieControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            EtatCategorieForm::class => EtatCategorieFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            EtatCategorieHydrator::class => EtatCategorieHydratorFactory::class
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            'etatcategorie'  => EtatCategorieViewHelper::class,

        ],
        'factories' => [
            EtatLegendeViewHelper::class => EtatLegendeViewHelperFactory::class,
        ],
        'aliases' => [
            'etatlegende' => EtatLegendeViewHelper::class,
        ]
    ],


];