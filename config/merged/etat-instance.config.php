<?php

namespace UnicaenEtat;

use UnicaenEtat\Command\EtatInstancePurgeCommand;
use UnicaenEtat\Command\EtatInstancePurgeCommandFactory;
use UnicaenEtat\Controller\EtatInstanceController;
use UnicaenEtat\Controller\EtatInstanceControllerFactory;
use UnicaenEtat\Provider\Privilege\EtatPrivileges;
use UnicaenEtat\Service\EtatInstance\EtatInstanceService;
use UnicaenEtat\Service\EtatInstance\EtatInstanceServiceFactory;
use UnicaenEtat\View\Helper\EtatInstanceViewHelper;
use UnicaenPrivilege\Guard\PrivilegeController;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => EtatInstanceController::class,
                    'action' => [
                        'rechercher-utilisateur',
                    ],
                    'roles' => [],
                ],
                [
                    'controller' => EtatInstanceController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        EtatPrivileges::ETAT_INDEX,
                    ],
                ],
                [
                    'controller' => EtatInstanceController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        EtatPrivileges::ETAT_DETRUIRE,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-etat' => [
                'child_routes' => [
                    'etat-instance' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/etat-instance',
                            'defaults' => [
                                'controller' => EtatInstanceController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'rechercher-utilisateur' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/rechercher-utilisateur',
                                    'defaults' => [
                                        'controller' => EtatInstanceController::class,
                                        'action' => 'rechercher-utilisateur',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:instance',
                                    'defaults' => [
                                        'controller' => EtatInstanceController::class,
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            EtatInstanceService::class => EtatInstanceServiceFactory::class,
            EtatInstancePurgeCommand::class => EtatInstancePurgeCommandFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            EtatInstanceController::class => EtatInstanceControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [],
    ],
    'hydrators' => [
        'factories' => [],
    ],
    'view_helpers' => [
        'invokables' => [
            'etatinstance' => EtatInstanceViewHelper::class,
        ]
    ],
    'laminas-cli' => [
        'commands' => [
            'etatInstance:purge' => EtatInstancePurgeCommand::class,
        ],
    ],

];