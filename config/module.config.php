<?php

use Doctrine\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Laminas\Router\Http\Literal;
use UnicaenEtat\Controller\EtatTypeController;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'doctrine' => [
        'driver' => [
            'orm_default' => [
                'class' => MappingDriverChain::class,
                'drivers' => [
                    'UnicaenEtat\Entity\Db' => 'orm_default_xml_driver',
                ],
            ],
            'orm_default_xml_driver' => [
                'class' => XmlDriver::class,
                'cache' => 'apc',
                'paths' => [
                    __DIR__ . '/../src/UnicaenEtat/Entity/Db/Mapping',
                ],
            ],
        ],
        'cache' => [
            'apc' => [
                'namespace' => 'UNICAEN-ETAT__' . __NAMESPACE__,
            ],
        ],
    ],

    'router'          => [
        'routes' => [
            'unicaen-etat' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/unicaen-etat',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => false,
                'child_routes' => [],
            ],
        ],
    ],

    'view_manager'    => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],

    'view_helpers'  => [
        'aliases' => [
        ],
        'factories' => [
        ],
    ],
    'public_files' => [
        'stylesheets' => [
            '10000_etat' => 'css/unicaen-etat.css',
        ],
    ],
];