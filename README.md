Bibliothèque unicaen/etat
===

La bibliothèque **unicaen/etat** est module proposant de gérer des états sur des entités.

La classe *EtatCategorie*
---
Permet de regrouper des types d'états portant sur des mêmes entités.

Une catégorie d'état est constitué :
* d'un `code` unique permettant de récupérer les type d'états allant ensemble (idéalement regroupé dans un provider) ;
* d'un `libelle` décrivant la catégorie d'état elle-même ;
* d'une `icone` à afficher lors du rendu;
* d'une `couleur` pour les distinguer dans l'affichage;
* d'un `ordre` pour gerer des priorités d'affichage;

La classe *EtatType*
---

Un type d'état est constitué :
* d'un `code` unique permettant de récupérer simplement un type (idéalement regroupé dans un provider) ;
* d'une `catégorie` permettant de regrouper les état portant sur une même entité ;
* d'un `libelle` décrivant le type de validation elle-même ;
* d'une `icone` à afficher lors du rendu;
* d'une `couleur` pour les distinguer dans l'affichage;
* d'un `ordre` pour gerer des priorités d'affichage;


La classe *EtatInstance*
---

Une instance d'état représente pour une entité donnée sont état actuel (ou passé) :
* d'un `type` d'état (voir `EtatType`) ;
* d'un champ optionnel `infos` permettant de fournir des explications sur les raisons de l'état ;

De plus les Instance sont munis de l'interface/trait `HistoriqueAware` permettant de gerer l'état actif et l'historique des états.


L'interface *HasEtatsInterface* et le trait *HasEtatsTrait*
---

L'interface et le trait `HasEtats` va ajouter une collection `etats` (qu'il faut initialiser dans le constructeur) qui sera en charge de stocker les succession d'états.
`HasEtats` fournit les méthodes suivantes :

```php
    public function getEtats(): ?Collection;
    public function getEtatsByCode(string $code, bool $withHisto) : ?Collection;
    public function getEtatActif(): ?EtatInstance;
    public function isEtatActif(string $code, ?DateTime $date = null) : bool;
    public function addEtat(EtatInstance $etat): void;
    public function removeEtat(EtatInstance $etat): void;
    public function clearEtats(): void;
    static public function decorateWithEtats(QueryBuilder $qb, string $entityName,  array $etats = []) : QueryBuilder;
    static public function decorateWithEtatsCodes(QueryBuilder $qb, string $entityName,  array $etats = []) : QueryBuilder;
```

L'interface *HasEtatInterface* et le trait *HasEtatTrait*
---
L'interface et le trait `HasEtat` va ajouter une instance d'état `etat`. Elle permet de gerer un état sans accés à l'historique.
`HasEtat` fournit les méthodes suivantes :

```php
    public function getEtat(): ?EtatInstance;
    public function setEtat(?EtatInstance $etat): void;
```
Remarques :`
___`
- Une entité ne posséde qu'un état actif (celui qui n'est pas archivé).
- Pour changer l'état d'une entité, il vaut mieux utiliser `EtatInstanceService.setEtatActif()` qui en cas de changement de type d'état se charge d'historiser l'ancien état et de creer l'instance du nouvelle état.
- Les instances d'états seront stockées dans la table `unicaen_etat_instance` et il est nécessaire de créer en linker `entity_etat` pour l'usage de 'HasEtats' ou d'un champs etat_id dans l'entité pour le cas de 'hasEtat'.
- En cas de suppression de l'entités en BDD, cela vide le linker mais ne détruit l'instance d'état (qui n'est plus liée à aucune données et n'est pas archivé)

Changements
===
- a faire : un viewHelper affichant l'historique des états.
- a faire : api qui retourne les instances d'états
- a faire : améliorer la commande etatInstance:purge afin de prendre de mieux gerer les types/catégories à purger

Scripts et description des tables
===
*Les scripts de création sont disponibles dans le répertoire `documentation`.*

Table `unicaen_etat_categorie` stocke la liste des catégories d'état.

| Attribut            | Type         | Remarque                                                     | 
|---------------------|--------------|--------------------------------------------------------------| 
| id                  | serial       | clef primaire                                                |
| code                | varchar(256) | code unique facilitant la récupération d'une catégorie donné |
| libelle             | varchar(256) | libellé associé à la catégorie                               |
| icone               | varchar(256) | icone associé à la catégorie                                 |
| couleur             | varchar(256) | couleur associé à la catégorie                               |
| ordre               | integer      | Ordre d'affichage de la catégorie                            |  

Table `unicaen_etat_type` stocke la liste des types d'état'.

| Attribut              | Type         | Remarque                                                   | 
|-----------------------|--------------|------------------------------------------------------------| 
| id                    | serial       | clef primaire                                              |
| code                  | varchar(256) | code unique facilitant la récupération d'un type donné     |
| libelle               | varchar(256) | libellé associé à la catégorie                             |
| categorie_id          | integer      | clef étrangère vers `unicaen_etat_categorie                |
| icone                 | varchar(256) | icone associé au type d'état                               |
| couleur               | varchar(256) | couleur associé au type d'état                             |
| ordre                 | integer      | Ordre d'affichage du type d'état                           |  


Table `unicaen_validation_instance` stocke la liste des types de validation.

| Attribut     | Type    | Remarque                                                                 | 
|--------------|---------|--------------------------------------------------------------------------| 
| id           | serial  | clef primaire                                                            |
| type_id      | integer | clef étrangère vers `unicaen_etat_type`                                  |
| infos        | text    |                                                                          |  
**Remarque :** L'entité associée est historisable et la table est munie des attributs associés.

ViewHelper
===
`EtatCategorieViewHelper` permet de gerer l'affichage de la catégorie sous forme de badge

`EtatTypeViewHelper` permet de gerer l'affichage de l'état sous forme de badge
- 'display-categorie' affiche la partie catégorie default:true; 
- 'display-type' affiche la partie type default:true;
- 'unknown-icon' : icone si non définie | défault: fas fa-question;
- 'unknown-color' : couleur si non définie | défault: gray;
- 'unknown-type-libelle' : Libelle du type si l'état type n'existe pas | défault: 'Aucun état';
- 'unknown-categorie-libelle' : Libelle de la catégorie si l'état type n'existe pas | défault: 'Aucun état';

Pour `display-categorie`, Cf `EtatCategorieViewHelper`

`EtatInstanceViewHelper` permet de gerer l'affichage de base d'un état avec différentes options
- 'display-date' : affiche de la date du passage à l'état | default:true
- 'display-user' : affiche de qui a modifier en dernier l'état | default:true;

Si les 2 options sont à false, on affiche l'état sous forme de badge (Cf EtatTypeViewHelper)

Purge des instances d'états historisées
---
La commande console `etatInstance:purge` permet de supprimer les instances d'états `historisées` depuis un certains temps.

- La durée par défaut de conservertion est de 1 an, il s'agit d'un paramétre modifiable dans la configuration sous la forme de DateInterval :
```
    'unicaen-etat' => [
        'conservation-time' => new DateInterval('P2Y'),
    ],
```
3 options possibles :
- `--time=P2Y` *(ou `-t P2Y`)* pour spécifier la durée de conservations des états (on ignore alors celle définie en conf).
- `--categorie=XXX`,  *(ou `-t XXX`)* pour ne purger que les instances d'états historisées de la catégorie dont le code est `XXX`
- `--type=YYY`,  *(ou `-t YYY`)* pour ne purger que les instances d'états historisées du type dont le code est `XXX`


Troubleshooting
===
- Pour nettoyer les instances d'état qui ne sont plus liées à rien, la procédure clear_etat_instance() est un exemple. 
- HistoCreateur/Modificateur/Destructeur prend l'utilisateur connecté lors du changement de statut ou par défaut le créateur de l'état précédent ce qui est potentiellement faux.